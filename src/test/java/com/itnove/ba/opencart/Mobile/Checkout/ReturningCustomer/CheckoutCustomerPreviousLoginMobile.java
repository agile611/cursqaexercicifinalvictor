package com.itnove.ba.opencart.Mobile.Checkout.ReturningCustomer;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import com.itnove.ba.opencart.Pages.CheckoutPageMobile;
import com.itnove.ba.opencart.Pages.LoginPage;
import com.itnove.ba.opencart.Pages.LoginPageMobile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutCustomerPreviousLoginMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
    //LoginMobile previ
        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Fem login
        LoginPageMobile login = new LoginPageMobile(driver);
        login.Login(login.valid_email, login.valid_password, driver);

     //Ara farem compra i checkout
        //Tornem a l'inici
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse al botó Add to Cart del Macbook, de la portada
        CheckoutPageMobile checkout = new CheckoutPageMobile(driver);
        Actions hover = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOf(checkout.addToCartMac));
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse a sobre de Checkout
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Clicar Checkout
        checkout.checkoutButton.click();

        //MOuse over Checkout 2
        hover.moveToElement(checkout.checkoutButton2).build().perform();

        //Clic al boto
        checkout.checkoutButton2.click();

        //Veure que apareix la seccio de Billing details directament
        wait.until(ExpectedConditions.visibilityOf(checkout.addressDisplay));
        assertTrue(checkout.addressDisplay.isDisplayed());


    }

}
