package com.itnove.ba.opencart.Mobile.Checkout.Convidat;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import com.itnove.ba.opencart.Pages.CheckoutPageMobile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutConvidatSenseNomMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre el botó Add to Cart del producte Macbook de la portada de la botiga.
        CheckoutPageMobile checkout = new CheckoutPageMobile(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar el botó Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Fer clic a checkout
        checkout.checkoutButton.click();

        //MOuse over Checkout 2
        hover.moveToElement(checkout.checkoutButton2).build().perform();

        //Clic al boto
        checkout.checkoutButton2.click();

        //Clicar a la opció Guest checkout
        wait.until(ExpectedConditions.elementToBeClickable(checkout.radioGuest));
        checkout.radioGuest.click();

        //Situar el mouse al botó continue
        hover.moveToElement(checkout.continueGuest).build().perform();

        //Fer clic al botó Continue
        checkout.continueGuest.click();

        //Clicar al camp de text Last Name
        wait.until(ExpectedConditions.elementToBeClickable(checkout.textLastName));
        checkout.textLastName.click();

        //Escriure Doe
        checkout.textLastName.sendKeys("Doe");

        //Clicar al camp de text email
        checkout.textEmail.click();

        //Escriure prova@prova.com
        checkout.textEmail.sendKeys("prova@prova.com");

        //Clicar al camp de text Telephone
        checkout.textPhone.click();

        //Escriure 777777777
        checkout.textPhone.sendKeys("777777777");

        //Clicar al camp de text Address1
        checkout.textAddress1.click();

        //Escriure Test address 1
        checkout.textAddress1.sendKeys("Test address 1");

        //Clicar al camp de text City
        checkout.textCity.click();

        //Escriure Barcelona
        checkout.textCity.sendKeys("Barcelona");

        //Clicar al camp de text Post Code
        checkout.textPostCode.click();

        //Escriure 08080
        checkout.textPostCode.sendKeys("08080");

        //Fer clic al desplegable Country
        checkout.countryDisplay.click();

        //CLicar Spain
        checkout.countrySpain.click();

        //Fer clic al desplegable Region / State
        checkout.regionDisplay.click();

        //CLicar Barcelona
        checkout.regionBarcelona.click();

        //Situar Mouse sobre botó Continue
        hover.moveToElement(checkout.continueToPaymentMethod).build().perform();

        //Clicar Continue
        checkout.continueToPaymentMethod.click();

        //Ha d'apareixer error a First Name
        wait.until(ExpectedConditions.visibilityOf(checkout.errorFirstName));
        assertTrue(checkout.errorFirstName.getText().equals("First Name must be between 1 and 32 characters!"));




    }

}
