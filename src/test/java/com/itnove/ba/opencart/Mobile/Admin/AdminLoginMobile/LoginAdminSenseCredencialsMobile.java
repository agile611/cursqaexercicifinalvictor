package com.itnove.ba.opencart.Mobile.Admin.AdminLoginMobile;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.LoginAdminPage;
import com.itnove.ba.opencart.Pages.LoginAdminPageMobile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LoginAdminSenseCredencialsMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/admin
        driver.get("http://opencart.votarem.lu/admin");

        //Fem el login
        LoginAdminPageMobile login = new LoginAdminPageMobile(driver);
        login.Login("", "", driver);

        //Verificar que no s'ha fet el login correctament
        wait.until(ExpectedConditions.visibilityOf(login.loginError));
        assertTrue(login.loginError.getText().contains("No match"));



    }

}
