package com.itnove.ba.opencart.Mobile.Checkout.Convidat;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import com.itnove.ba.opencart.Pages.CheckoutPageMobile;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutCarroBuitMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        CheckoutPageMobile checkout = new CheckoutPageMobile(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Clicar checkout
        checkout.checkoutButton.click();

        //Verifiquem que no apareix cap resultat
        assertTrue(checkout.missatgeCarroBuit.getText().equals("Your shopping cart is empty!"));

    }

}
