package com.itnove.ba.opencart.Mobile.Admin.ForgotPassword;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.LoginAdminPage;
import com.itnove.ba.opencart.Pages.LoginAdminPageMobile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class ForgotPasswordMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu/admin");

        //Clicaremo Forgotten Password
        LoginAdminPageMobile login = new LoginAdminPageMobile(driver);
        login.forgotPass.click();

        //CLicar el camp de text Email Address
        wait.until(ExpectedConditions.visibilityOf(login.textEmail));
        login.textEmail.click();

        //Escriure un email invalid
        login.textEmail.sendKeys("test@test.com");

        //Situarse al boto de reset
        Actions hover = new Actions(driver);
        hover.moveToElement(login.resetButton).build().perform();

        //CLicar reset
        login.resetButton.click();

        //Verificar que s'ha fet el login correctament
        wait.until(ExpectedConditions.visibilityOf(login.errorReset));
        assertTrue(login.errorReset.getText().contains("The E-Mail Address was not found"));



    }

}
