package com.itnove.ba.opencart.Desktop.Checkout.Convidat;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutConvidat extends BaseTest {

    private String nom = "John";
    private String cognom = "Doe";
    private String email = "John@email.com";
    private String telefon = "777777777";
    private String address = "xxxx x xxxx x x x";
    private String ciutat = "Barcelona";
    private String CodiPostal = "08020";


    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre el botó Add to Cart del producte Macbook de la portada de la botiga.
        CheckoutPage checkout = new CheckoutPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar el botó Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Fer clic a checkout
        checkout.checkoutButton.click();

        //Clicar a la opció Guest checkout
        wait.until(ExpectedConditions.elementToBeClickable(checkout.radioGuest));
        checkout.radioGuest.click();

        //Situar el mouse al botó continue
        hover.moveToElement(checkout.continueGuest).build().perform();

        //Fer clic al botó Continue
        checkout.continueGuest.click();

        //Omplir camps
        wait.until(ExpectedConditions.elementToBeClickable(checkout.textName));
        checkout.omplirCamps(nom, cognom, email, telefon, address, ciutat, CodiPostal, driver);

        //Verificar que surt error de metode de pagament
        wait.until(ExpectedConditions.elementToBeClickable(checkout.errorPaymentMethod));
        assertTrue(checkout.errorPaymentMethod.isDisplayed());



    }

}
