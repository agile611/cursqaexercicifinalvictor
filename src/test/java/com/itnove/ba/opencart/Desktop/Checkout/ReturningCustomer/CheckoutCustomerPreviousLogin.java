package com.itnove.ba.opencart.Desktop.Checkout.ReturningCustomer;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import com.itnove.ba.opencart.Pages.LoginPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutCustomerPreviousLogin extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
    //LoginMobile previ
        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Fem login
        LoginPage login = new LoginPage(driver);
        login.Login(login.valid_email, login.valid_password, driver);

     //Ara farem compra i checkout
        //Tornem a l'inici
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse al botó Add to Cart del Macbook, de la portada
        CheckoutPage checkout = new CheckoutPage(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse a sobre de Checkout
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Clicar Checkout
        checkout.checkoutButton.click();

        //Veure que apareix la seccio de Billing details directament
        wait.until(ExpectedConditions.visibilityOf(checkout.addressDisplay));
        assertTrue(checkout.addressDisplay.isDisplayed());


    }

}
