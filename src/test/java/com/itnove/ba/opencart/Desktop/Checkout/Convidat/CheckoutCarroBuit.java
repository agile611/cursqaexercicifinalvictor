package com.itnove.ba.opencart.Desktop.Checkout.Convidat;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutCarroBuit extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        CheckoutPage checkout = new CheckoutPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Clicar checkout
        checkout.checkoutButton.click();

        //Verifiquem que no apareix cap resultat
        assertTrue(checkout.missatgeCarroBuit.getText().equals("Your shopping cart is empty!"));

    }

}
