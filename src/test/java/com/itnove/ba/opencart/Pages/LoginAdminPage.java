package com.itnove.ba.opencart.Pages;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginAdminPage extends BaseTest {


        private WebDriver driver;

        public String valid_user = "user";
        public String invalid_user = "invaliduser";
        public String valid_password = "bitnami1";
        public String invalid_password = "invalidpassword";

        @FindBy(xpath="//*[@id='input-username']")
        public WebElement textUser;

        @FindBy(xpath="//*[@id='input-password']")
        public WebElement textPass;

        @FindBy(xpath = "//*[@id='content']/div/div/div/div/div[2]/form/div[2]/span/a")
        public WebElement forgotPass;

        @FindBy(xpath = "//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button")
        public WebElement loginAdmin;

        @FindBy(xpath = "//*[@id='modal-security']/div/div/div[1]/h4")
        public WebElement securityNotification;

        @FindBy(xpath = "//*[@id='content']/div/div/div/div/div[2]/div")
        public WebElement loginError;

        @FindBy(xpath = "//*[@id='input-email']")
        public WebElement textEmail;

        @FindBy(xpath = "//*[@id='content']/div/div/div/div/div[2]/form/div[2]/button")
        public WebElement resetButton;

        @FindBy(xpath = "//*[@id='content']/div/div/div/div/div[2]/div")
        public WebElement errorReset;
        //The E-Mail Address was not found


        public LoginAdminPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public void Login(String user, String password, WebDriver driver) {

                //LoginPage login = new LoginPage(driver);
                Actions hover = new Actions(driver);

                hover.moveToElement(textUser).build().perform();

                //Clicar el camp de text user
                textUser.click();

                //Escriure usuari valid
                textUser.sendKeys(user);

                //Clicar el camp de text password
                textPass.click();

                //Escriure password valid
                textPass.sendKeys(password);

                //Mouse over botó de LoginMobile
                hover.moveToElement(loginAdmin).build().perform();

                //Clic a LoginMobile
                loginAdmin.click();



        }
}

