package com.itnove.ba.crm.Desktop.GestioObjectes;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EliminarCompte extends BaseTest {

    public String accountName = "UsuariNouCrm";

    @Test
    public void deleteAccount() throws InterruptedException {
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.search(driver,wait,hover,accountName);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),accountName);
        searchResultsPage.clickOnFirstResult(wait);
        assertTrue(driver.getPageSource().toUpperCase().contains(accountName.toUpperCase()));
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
        editAccountPage.deleteAccount(driver, hover);
        System.out.println(accountName);
    }
}
