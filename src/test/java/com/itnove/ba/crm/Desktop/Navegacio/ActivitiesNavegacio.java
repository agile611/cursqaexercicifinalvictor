package com.itnove.ba.crm.Desktop.Navegacio;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ActivitiesNavegacio extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        dashboardPage.hoverMenus(driver, hover, dashboardPage.activitiesMenu,"/html/body/div[2]/nav/div/div[2]/ul/li[5]/span[2]/ul/li");
    }
}
