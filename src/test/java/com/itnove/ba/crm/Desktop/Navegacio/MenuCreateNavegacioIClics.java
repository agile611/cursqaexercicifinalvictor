package com.itnove.ba.crm.Desktop.Navegacio;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class MenuCreateNavegacioIClics extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        Thread.sleep(3000);
        dashboardPage.hoverAndClickEveryCreate(driver, hover);
    }
}
