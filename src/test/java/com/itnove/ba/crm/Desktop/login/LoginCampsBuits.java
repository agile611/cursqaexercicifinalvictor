package com.itnove.ba.crm.Desktop.login;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class LoginCampsBuits extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("","");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertFalse(LoginPage.errorPass.getText().equals("You must specify a valid username and password."));
        Assert.assertEquals("http://crm.votarem.lu/index.php?action=Login&module=Users", driver.getCurrentUrl());


    }
}
