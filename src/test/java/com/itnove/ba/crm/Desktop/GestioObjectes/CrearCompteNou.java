package com.itnove.ba.crm.Desktop.GestioObjectes;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

public class CrearCompteNou extends BaseTest {

    public String accountName = "UsuariNouCrm";

    @Test
    public void createAccount() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateAccountLink(hover);
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.fillName(accountName);
        createAccountPage.saveAccount();
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
     }

    //@Test(dependsOnMethods = "createAccount")

}
