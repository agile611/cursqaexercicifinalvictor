package com.itnove.ba.crm.Desktop.GestioObjectes;

import com.itnove.ba.BaseTest;


import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CrearContacteNou extends BaseTest {

    public String contactName = "ContacteNouCrm";

    @Test
    public void createAccount() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateContactLink(hover);
        CreateContactPage createcontactPage = new CreateContactPage(driver);
        createcontactPage.fillName(contactName);
        createcontactPage.saveAccount();
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(contactName.toUpperCase(), editAccountPage.getTitulo().toUpperCase());
    }
}