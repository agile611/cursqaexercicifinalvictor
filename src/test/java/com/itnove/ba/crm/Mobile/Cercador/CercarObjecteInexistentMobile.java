package com.itnove.ba.crm.Mobile.Cercador;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CercarObjecteInexistentMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        String keyword = "castanya";
        //Accedir a pagina
        driver.navigate().to("http://crm.votarem.lu");
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPageMobile dashboardPage = new DashboardPageMobile(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        Actions hover = new Actions(driver);
        dashboardPage.search(driver,wait,hover,keyword);
        SearchResultsPageMobile searchResultsPage = new SearchResultsPageMobile(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);
        assertTrue(searchResultsPage.isNoSearchResultsDisplayed(wait));
    }
}
