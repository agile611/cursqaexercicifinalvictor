package com.itnove.ba.crm.Mobile.Cercador;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.crm.pages.DashboardPageMobile;
import com.itnove.ba.crm.pages.LoginPage;
import com.itnove.ba.crm.pages.SearchResultsPageMobile;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CercarSenseTextMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        String keyword = "ripoll";
        //Accedir a pagina
        driver.navigate().to("http://crm.votarem.lu");
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("","");
        DashboardPageMobile dashboardPage = new DashboardPageMobile(driver);
        //Comprovo que arribo al dashboard
        Actions hover = new Actions(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.search(driver,wait,hover,keyword);
        SearchResultsPageMobile searchResultsPage = new SearchResultsPageMobile(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);
        //searchResultsPage.clickOnFirstResult(wait);
        assertFalse(driver.getPageSource().toUpperCase().contains(keyword.toUpperCase()));
    }
}
