package com.itnove.ba.crm.Mobile.login;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.DashboardPageMobile;
import com.itnove.ba.crm.pages.LoginPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LogoutTestMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        driver.get("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPageMobile dashboardPage = new DashboardPageMobile(driver);
        Actions hover = new Actions(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.logout(driver,wait,hover);
        Thread.sleep(3000);
        assertTrue(loginPage.isLoginButtonPresent(driver,wait));
    }
}
