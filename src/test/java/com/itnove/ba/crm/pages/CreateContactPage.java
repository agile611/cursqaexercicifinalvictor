package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateContactPage {

    private WebDriver driver;

    @FindBy(id = "last_name")
    public WebElement lastnameTextbox;

    @FindBy(id = "SAVE")
    public WebElement saveButton;

    public void fillName(String name){
        lastnameTextbox.sendKeys(name);
    }

    public void saveAccount(){
        saveButton.click();
    }

    public CreateContactPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
