package com.itnove.ba.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class DashboardPageMobile {

    private WebDriver driver;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[3]/li[1]/a")
    public WebElement createAccountLink;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[2]/li[5]/a")
    public WebElement createDocumentLink;

    @FindBy(xpath = "(//*[@id='quickcreatetop']/a)[3]")
    public WebElement createButton;

    @FindBy(id = "xstab0")
    public WebElement suiteCrmDashboard;

    @FindBy(xpath = "(//*[@id='usermenucollapsed'])[1]")
    public WebElement menuIcono;

    @FindBy(xpath = "(//*[@id='logout_link'])[1]")
    public WebElement logout;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actions;

    @FindBy(xpath = ".//*[@id='tab-actions']/ul/li[2]/input")
    public WebElement addTab;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[3]/div/div/div[3]/button[2]")
    public WebElement addTabWidgetButton;

    @FindBy(xpath = "(//*[@id='searchbutton'])[1]")
    public WebElement lupa;

    @FindBy(xpath = "(//*[@id='query_string'])[1]")
    public WebElement searchBox;

    @FindBy(xpath = "(//*[@id='searchformdropdown']/div/span/button)[1]")
    public WebElement iconoLupa;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[2]")
    public WebElement salesMenu;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[3]")
    public WebElement marketingMenu;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[4]")
    public WebElement supportMenu;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[5]")
    public WebElement activitiesMenu;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[6]")
    public WebElement collabMenu;

    @FindBy(xpath = "//*[@id='toolbar']/ul/li[7]")
    public WebElement allMenu;

    @FindBy(xpath = "(//*[@id='quickcreatetop']/a)[3]")
    public WebElement createItems;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[3]/li[2]/a")
    public WebElement createContactLink;



    public void createButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createButton)
                .click().build().perform();
    }

    public void clickOnCreateAccountLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createAccountLink)
                .click().build().perform();
    }

    public void clickOnCreateContactLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createContactLink)
                .click().build().perform();
    }

    public void menuClick(Actions hover, WebElement menu) {
        hover.moveToElement(menu)
                .moveToElement(menu)
                .click().build().perform();
    }

    public void hoverAndClickEveryCreate(WebDriver driver, Actions hover) throws InterruptedException {
        createButtonClick(hover);
        String listElements = "(.//*[@id='quickcreatetop']/ul)[3]/li";
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            createButtonClick(hover);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(createButton)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
        }
    }


    public void hoverMenus(WebDriver driver, Actions hover, WebElement menu, String xpath) throws InterruptedException {
        menuClick(hover, menu);
        String listElements = xpath;
        //String lsl = listElements + "/li";
        //System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(listElements));
        for (int i = 1; i <= listOfCreates.size(); i++) {

            menuClick(hover, menu);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]"));
            System.out.println(listElements + "[" + i + "]");
            hover.moveToElement(menu)
                    .moveToElement(eachCreateItem)
                    .build().perform();
            Thread.sleep(1000);
        }
    }

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboard));
        return suiteCrmDashboard.isDisplayed();
    }

    public void logout(WebDriver driver, WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(menuIcono)
                .moveToElement(menuIcono)
                .click().build().perform();
        hover.moveToElement(menuIcono).moveToElement(logout).moveToElement(logout).click().build().perform();

    }

    public void addTab(WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(actions)
                .moveToElement(actions)
                .click().build().perform();
        hover.moveToElement(actions)
                .moveToElement(addTab)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(addTabWidgetButton));
        hover.moveToElement(addTabWidgetButton)
                .moveToElement(addTabWidgetButton)
                .click().build().perform();
    }

    public void search(WebDriver driver, WebDriverWait wait, Actions hover, String keyword) throws InterruptedException {
        hover.moveToElement(lupa)
                .moveToElement(lupa)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.clear();
        searchBox.sendKeys(keyword);
        hover.moveToElement(iconoLupa)
                .moveToElement(iconoLupa)
                .click().build().perform();
    }

    public DashboardPageMobile(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
