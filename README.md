# Exercici final Barcelona Activa Desembre 2017

## Support

This tutorial is released into the public domain by ITNove under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact ITNove for further details.

* ITNOVE a Cynertia Consulting
* Passeig de Gràcia 110, 4rt 2a
* 08008 Barcelona
* T: 93 184 53 44